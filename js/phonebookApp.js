(function() {
   'use strict';
   angular
      .module('phonebookApp', [
            'ui.bootstrap'
        ]);
})();

/*
var phonebookApp = angular.module('phonebookApp', [])

phonebookApp.controller('PhonebookListCtrl', function($scope) {
  $scope.name = '';
  $scope.phone = '';
  $scope.email = '';
  $scope.enable = '';
  $scope.group = '';
  $scope.last_call = '';
  
  
  $scope.orderProp = 'id';

  $scope.edit = true;
  $scope.error = false;
  $scope.incomplete = false;
  
$scope.addContact =function() {
    $scope.phonebooks.push({
    name: $scope.name, 
    phone: $scope.phone, 
    email: $scope.email, 
    group: $scope.group,
    enable: $scope.enable
    
    }); 

  
  $scope.name = '';
  $scope.phone = '';
  $scope.email = '';
  $scope.group = '';
  $scope.enable = '';
  
  };
  
  $scope.editUser = function(id) {
    if (id == 'new') {
      $scope.edit = true;
      $scope.incomplete = true;
      $scope.name = '';
      $scope.phone = '';
      $scope.email = '';
      $scope.group = '';
      $scope.enable = '';
      
    } else {
      $scope.edit = true;
      $scope.name = $scope.phonebooks[id - 1].name;
      $scope.phone = $scope.phonebooks[id - 1].phone;
      $scope.email = $scope.phonebooks[id - 1].email
      $scope.group = $scope.phonebooks[id - 1].group;
      $scope.enable = $scope.phonebooks[id - 1].enable;
    }

  };
  $scope.$watch('group', function() {
    $scope.test();
  });
  $scope.$watch('name', function() {
    $scope.test();
  });
  $scope.$watch('phone', function() {
    $scope.test();
  });
  $scope.$watch('email', function() {
    $scope.test();
  });
  $scope.$watch('enable', function() {
    $scope.test();
  });
  
  $scope.test = function() {
    $scope.incomplete = false;
    if ($scope.edit && (!$scope.name.length ||
        !$scope.name.length ||
        !$scope.email.length ||
		    !$scope.group.length)) {
      $scope.incomplete = true;
    }
  };
 
});
*/
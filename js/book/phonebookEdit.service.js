
(function () {
    'use strict';
    angular
        .module('phonebookApp')
        .service('modalWindow', modalWindow);

    modalWindow.$inject = ['$uibModal'];

    function modalWindow($uibModal) {
        var modalWindowFactory = {};
        
        //Edit
        modalWindowFactory.edit = function (book) {
            
            console.log('Service : funcEdit');

            return $uibModal.open({
                templateUrl: 'tmpl/EditBook.htm',
                controller: 'phonebookEdit',
                controllerAs: 'new_book11',
                resolve: {
                    book_edit_param: function () {
                        return angular.copy(book);
                    }
                }
            });

        };
        
        //Add
        modalWindowFactory.create = function () {

            console.log('Service : funcCreate');

            return $uibModal.open({
                templateUrl: 'tmpl/EditBook.htm',
                controller: 'phonebookEdit',
                controllerAs: 'new_book11',
                resolve: {
                    book_edit_param: function () {
                        //return angular.copy(book);
                    }
                }
            });

        };

        //Delete
        modalWindowFactory.delete = function (book) {

            console.log('Service : funcDelete');

            return $uibModal.open({
                templateUrl: 'tmpl/deleteBook.htm',
                controller: 'phonebookEdit',
                controllerAs: 'del_book',
                resolve: {
                    book_edit_param: function () {
                        //return angular.copy(book);
                    }
                }
            });

        };

        return modalWindowFactory;
    }
})();

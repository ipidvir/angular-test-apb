(function () {
    'use strict';
    angular
        .module('phonebookApp')
        .controller('phonebookEdit', phonebookEdit);

    phonebookEdit.$inject = ['$scope', '$uibModalInstance', 'book_edit_param'];

    function phonebookEdit($scope, $uibModalInstance, book_edit_param) {
        var vm = this;
        console.log('PhonebookEdit: Edit Controller -start.');
        vm.book = book_edit_param;

        vm.ok = function () {
            $uibModalInstance.close(vm.book);
            console.log('Press "OK"');
            console.log('Press "OK"');

        };

        vm.cancel = function () {
            $uibModalInstance.dismiss('cancel');
            console.log('Press "Cancel"');
        };

        vm.delete = function () {
            $uibModalInstance.close(null);
            console.log('Press "Delete"');
        };
        console.log('PhonebookEdit: Edit Controller -end.');

    }
})();
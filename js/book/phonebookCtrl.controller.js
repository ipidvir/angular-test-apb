(function() {
    'use strict';
    angular
        .module('phonebookApp')
        .controller('phonebookCtrl', phonebookCtrl);
    /*phonebookCtrl.$inject = [];*/
    phonebookCtrl.$inject = ['$scope', 'modalWindow'];

    /* @ngInject */
    function phonebookCtrl($scope, modalWindow) {
        var vm = this;
        console.log('Controller start!!!');
        vm.title = 'phonebookCtrl';
        //vm.searchQuery = '';
        //vm.reverseSort = false;

        vm.phonebookList = [
          {
            'name': 'Jean-Claude Van Damme',
            'email': 'jcvd@gmail.com',
            'image': {
                  orgn: 'img/prod1.jpg',
                  thumb: 'img/prod1_thumb.jpg'
               },
            'phone': '+38973332211',
            'group': 'Work',
            'enable': true,
            'id': '1'
          }, {
            'name': 'Jackie Chan',
            'email': 'jchan@gmail.com',
            'image': {
                  orgn: 'img/prod1.jpg',
                  thumb: 'img/prod1_thumb.jpg'
               },
            'phone': '+38639998877',
            'group': 'VIP',
            'enable': true,
            'id': '2'
          }, {
            'name': 'Taylor Swift',
            'email': 'tswift@gmail.com',
            'image': {
                  orgn: 'img/prod1.jpg',
                  thumb: 'img/prod1_thumb.jpg'
               },
            'phone': '+380962001030',
            'group': 'Friends',
            'enable': true,
            'id': '3'
          }, {
            'name': 'Justin Bieber',
            'email': 'jdbieber@gmail.com',
            'image': {
                  orgn: 'img/prod1.jpg',
                  thumb: 'img/prod1_thumb.jpg'
               },
            'phone': '+380636000001',
            'group': 'Friends',
            'enable': true,
            'id': '4'
          }, {
            'name': 'Jim Carrey',
            'email': 'jimcarrey@gmail.com',
            'image': {
                  orgn: 'img/prod1.jpg',
                  thumb: 'img/prod1_thumb.jpg'
               },
            'phone': '+380961234567',
            'group': 'Work',
            'enable': true,
            'id': '5'
          }, {
            'name': 'Abraham Lincoln',
            'email': 'alink@gmail.com',
            'image': {
                  orgn: 'img/prod1.jpg',
                  thumb: 'img/prod1_thumb.jpg'
               },
            'phone': '+380503005021',
            'group': 'Friends',
            'enable': false,
            'id': '6'
          }, {
            'name': 'Steven Spielberg',
            'email': 'ss@gmail.com',
            'phone': '+380969696966',
            'image': {
                  orgn: 'img/prod1.jpg',
                  thumb: 'img/prod1_thumb.jpg'
               },
            'group': 'Work',
            'enable': true,
            'id': '7'
          }, {
            'name': 'Adam Sandler',
            'email': 'adamek@gmail.com',
            'image': {
                  orgn: 'img/prod1.jpg',
                  thumb: 'img/prod1_thumb.jpg'
               },
            'phone': '+380965544333',
            'group': 'Family',
            'enable': true,
            'id': '8'
          }

        ];

        vm.openModalEdit = function (index, book) {
           console.log('openModal : Edit -start.');
            var modalInstance = modalWindow.edit(book);
 
            modalInstance.result.then(function (book_edit) {
               console.log( "Edit:" + book_edit.name);
               vm.phonebookList[index] = book_edit;
            });
           console.log('openModal : Edit -end.');
        };

        vm.openModalAdd = function () {
           console.log('openModal : Add -start.');
            var modalInstance = modalWindow.create();

            modalInstance.result.then(function (book_new) {
               console.log( "Add New PhoneBook:" + book_new.name);
               
               //console.log('ENABLE:' + book_new.enable );

               vm.phonebookList.push({
                  name: book_new.name, 
                  email: book_new.email, 
                  image: {
                     orgn: 'img/prod1.jpg',
                     thumb: 'img/prod1_thumb.jpg'
                     },
                  phone: book_new.phone, 
                  group: book_new.group,                  
                  enable: book_new.enable
                  }); 
            });
            console.log('openModal : Add -end.');            
        };

        vm.openModalDelete = function (book) {
           console.log('openModal : Delete -start.');
            var modalInstance = modalWindow.delete(book);

            modalInstance.result.then(function (book_del) {
               //console.log( "Delete: id=" + book_del.name);
               
               for(var i = vm.phonebookList.length - 1; i >= 0; i--){
                   if(vm.phonebookList[i].id == book.id){
                       vm.phonebookList.splice(i,1);
                   }
               }
            });
            console.log('openModal : Delete -end.');
        };

    }
})();